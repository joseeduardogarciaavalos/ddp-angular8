import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { db,Translation,APP_CONFIG_VALUE } from "./app.config2";

export class TranslationLoader implements TranslateLoader {
    constructor(private http: HttpClient) {}
    
    getTranslation(lang: string): Observable<any> {
      const promise = db.translations.where("lang").equals(lang).toArray().then( res => {
        if( res.length === 0) 
          return this.http.get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + "/api/translation?lang=" + lang).toPromise().then( apiRes => {
            console.log("Traducción solicitada: ", apiRes);
            db.translations.bulkAdd(apiRes);
            return apiRes;
          });
        return res;  
      }).then((traducciones)=>{
        console.log("Traducciones cargadas de IndexdDB: ", traducciones);
        return traducciones;
      }).then(traducciones => {
        return traducciones.map((t) => ({[t.key]: t.value}));
      });
      return from(promise).pipe(
        mergeMap(e => from(e)),
      );
    }
  }
  
export function HttpLoaderFactory(http: HttpClient) {
return new TranslationLoader(http);
}

@NgModule({
    imports: [
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
        }
      })
    ],
    exports: [ TranslateModule]
  })
export class TraductorModule { }
  