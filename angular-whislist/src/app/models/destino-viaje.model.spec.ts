import * as NgRx from './destino-viajes-state.model';
import { DestinoViaje } from './destino-viaje.models';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        //setup (Preparación)
        const preEstado: NgRx.Estado = NgRx.inicialEstado;
        const accion: NgRx.Inicial = new NgRx.Inicial(['destino 1', 'destino 2']);
        //action (Acción)
        const postEstado: NgRx.Estado = NgRx.reducer(preEstado, accion);
        // assert (Verificación)
        expect(postEstado.destinos.length).toEqual(4);
        expect(postEstado.destinos[2].nombre).toEqual('destino 1');
        // tear down (Demolición de efectos colaterales) Como borrar un registro mock de la BD
    });

    it('should reduce new item added', () => {
        const preEstado: NgRx.Estado = NgRx.inicialEstado;
        const accion: NgRx.Nuevo = new NgRx.Nuevo(new DestinoViaje('barcelona', 'url'));
        const postEstado: NgRx.Estado = NgRx.reducer(preEstado, accion);
        expect(postEstado.destinos.length).toEqual(3);
        expect(postEstado.destinos[2].nombre).toEqual('barcelona');
    });
});