describe("Ventana principal", () => {
    it("1. Tiene encabezado correcto y en español por defecto", () =>{
        cy.visit("localhost:4200");
        cy.contains("Destinos");
        cy.get("h5").should("contain","Hola");
    })
    it("2. Se puede ver un destino al agregarse", () =>{
        cy.get('#nombre').click().type('Costa Rica', { delay: 100 });
        cy.get('form button').click()
        cy.get("div h3").should("contain","Costa Rica");
    })
    it("3. Se puede cambiar de idioma (español=>inglés)", () =>{
        cy.get('select').select('en')
        cy.get("h5").should("contain","Hello");
    })
});